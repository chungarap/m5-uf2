package Solucion;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class AutoChessTest{

	@Test
	public void Test_Calcular_da�o_1_1() {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(5);
		Puntos_vida.add(1);
		Puntos_vida.add(2);
		Puntos_vida.add(7);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1812, res);
		
	}
	
	@Test
	public void Test_Pieza_atacada_1_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,4},
							{0,0,0,0,0},
							{0,5,7,0,0},
							{1,2,0,0,0},

		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(5, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_1_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,4},
							{0,0,0,0,0},
							{0,5,7,0,0},
							{1,2,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(5);
		Expected.add(7);
		Expected.add(1);
		Expected.add(2);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Calcular_da�o_1_2() {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(3);
		Puntos_vida.add(4);
		Puntos_vida.add(7);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1928, res);
		
	}
	
	@Test
	public void Test_Pieza_atacada_1_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,4,0,2},
							{7,8,3,0,0},
							{0,0,0,6,0},
							{0,0,1,0,0},
		
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_1_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,4,0,2},
							{7,8,3,0,0},
							{0,0,0,6,0},
							{0,0,1,0,0},

		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(4);
		Expected.add(3);
		Expected.add(7);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Calcular_da�o_2_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(1);
		Puntos_vida.add(7);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1380, res);
		
	}
	
	@Test
	public void Test_Pieza_atacada_2_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,4,0,2},
							{7,8,3,0,0},
							{0,0,0,6,0},
							{0,0,1,0,0},
		
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_2_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,1,0,0,0},
							{8,0,0,0,0},
							{0,7,4,0,6},
							{0,3,2,5,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(1);
		Expected.add(7);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Calcular_da�o_2_2 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(5);
		Puntos_vida.add(6);
		Puntos_vida.add(1);
		Puntos_vida.add(2);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(2388, res);
		
	}
	
	@Test
	public void Test_Pieza_atacada_2_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,3,0,0,0},
							{4,0,0,0,0},
							{0,0,5,6,0},
							{0,2,8,1,7},
		
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_2_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,3,0,0,0},
							{4,0,0,0,0},
							{0,0,5,6,0},
							{0,2,8,1,7},

		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(5);
		Expected.add(6);
		Expected.add(1);
		Expected.add(2);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Calcular_da�o_3_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(1);
		Puntos_vida.add(3);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1432, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_3_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,2,7},
							{0,1,0,0,6},
							{8,3,0,0,5},
							{0,0,4,0,0},

		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(1);
		Expected.add(3);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_3_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,2,7},
							{0,1,0,0,6},
							{8,3,0,0,5},
							{0,0,4,0,0},
		
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_3_2 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(3);
		Puntos_vida.add(4);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1488, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_3_2 () {
		
		Programa.lista();
		int [][] matriz = { {2,7,0,6,1},
							{0,0,0,3,5},
							{0,0,8,4,0},
							{0,0,0,0,0},

		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(3);
		Expected.add(4);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_3_2 () {
		
		Programa.lista();
		int [][] matriz = { {2,7,0,6,1},
							{0,0,0,3,5},
							{0,0,8,4,0},
							{0,0,0,0,0},
		
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_3_3 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(3);
		Puntos_vida.add(6);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1508, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_3_3 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,7,0,5},
							{0,1,0,0,0},
							{4,0,0,0,0},
							{0,6,8,3,2},

		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(3);
		Expected.add(6);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_3_3 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,7,0,5},
							{0,1,0,0,0},
							{4,0,0,0,0},
							{0,6,8,3,2},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	
	@Test
	public void Test_Calcular_da�o_4_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(3);
		Puntos_vida.add(1);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(912, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_4_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,3,0},
							{0,0,2,0,0},
							{0,4,0,0,0},
							{1,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(3);
		Expected.add(2);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_4_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,3,0},
							{0,0,2,0,0},
							{0,4,0,0,0},
							{1,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(3, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_5_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(5);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(508, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_5_1 () {
		
		Programa.lista();
		int [][] matriz = { {2,0,0,0,0},
							{0,0,0,0,0},
							{0,0,0,0,0},
							{5,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(5);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_5_1 () {
		
		Programa.lista();
		int [][] matriz = { {2,0,0,0,0},
							{0,0,0,0,0},
							{0,0,0,0,0},
							{5,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(5, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_5_2 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(5);
		Puntos_vida.add(4);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(984, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_5_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,0},
							{0,0,7,2,0},
							{0,4,0,0,0},
							{5,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(5);
		Expected.add(4);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_5_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,0},
							{0,0,7,2,0},
							{0,4,0,0,0},
							{5,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(5, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_6_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(4);
		Puntos_vida.add(2);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(920, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_6_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,0},
							{0,4,0,0,0},
							{0,0,2,0,0},
							{1,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(4);
		Expected.add(2);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_6_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,0},
							{0,4,0,0,0},
							{0,0,2,0,0},
							{1,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(4, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_7_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(6);
		Puntos_vida.add(7);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(936, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_7_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,2},
							{0,0,0,0,0},
							{0,0,0,6,0},
							{1,0,0,7,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(6);
		Expected.add(7);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_7_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,2},
							{0,0,0,0,0},
							{0,0,0,6,0},
							{1,0,0,7,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(6, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_7_2 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(5);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(508, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_7_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,3,0,2},
							{0,0,0,0,0},
							{0,5,0,0,0},
							{0,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(5);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_7_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,3,0,2},
							{0,0,0,0,0},
							{0,5,0,0,0},
							{0,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(5, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_8_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(6);
		Puntos_vida.add(7);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(936, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_8_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,2},
							{0,0,0,0,0},
							{0,0,0,6,0},
							{1,0,0,7,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(6);
		Expected.add(7);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_8_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,2},
							{0,0,0,0,0},
							{0,0,0,6,0},
							{1,0,0,7,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(6, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_9_1 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(4);
		Puntos_vida.add(2);
		Puntos_vida.add(1);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(1860, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_9_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,3,0,0,0},
							{0,2,4,0,0},
							{0,0,8,1,0},
							{0,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(4);
		Expected.add(2);
		Expected.add(1);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_9_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,3,0,0,0},
							{0,2,4,0,0},
							{0,0,8,1,0},
							{0,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_9_2 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(6);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(496, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_9_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,3,0,0,0},
							{0,0,0,0,0},
							{0,0,0,6,0},
							{0,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(6);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_9_2 () {
		
		Programa.lista();
		int [][] matriz = { {0,3,0,0,0},
							{0,0,0,0,0},
							{0,0,0,6,0},
							{0,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(6, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_9_3 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(4);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(476, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_9_3 () {
		
		Programa.lista();
		int [][] matriz = { {0,4,0,0,0},
							{0,0,0,0,0},
							{0,0,0,2,0},
							{0,0,0,0,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(4);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_9_3 () {
		
		Programa.lista();
		int [][] matriz = { {0,4,0,0,0},
							{0,0,0,0,0},
							{0,0,0,2,0},
							{0,0,0,0,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(4, res);
		
	}
	
	@Test
	public void Test_Calcular_da�o_10 () {
		
		Programa.lista();
		ArrayList <Integer> Puntos_vida= new ArrayList <Integer>();
		Puntos_vida.add(8);
		Puntos_vida.add(4);
		Puntos_vida.add(2);
		Puntos_vida.add(1);
		Puntos_vida.add(6);
		Puntos_vida.add(7);
		Puntos_vida.add(3);
		Puntos_vida.add(5);
		int res= Programa.Calcular_da�o(Puntos_vida);
		assertEquals(3796, res);
		
	}
	
	@Test
	public void Test_Zona_impacto_10_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,0},
							{0,2,4,0,0},
							{0,6,8,1,0},
							{0,7,3,5,0},
		};
		ArrayList <Integer> Expected= new ArrayList <Integer>();
		Expected.add(8);
		Expected.add(4);
		Expected.add(2);
		Expected.add(1);
		Expected.add(6);
		Expected.add(7);
		Expected.add(3);
		Expected.add(5);
		ArrayList <Integer> Retorn= new ArrayList <Integer>();
		Retorn= Programa.Zona_Impacto(matriz, Programa.Pieza_Atacada(matriz));
		assertEquals(Expected, Retorn);
		
	}
	
	@Test
	public void Test_Pieza_atacada_10_1 () {
		
		Programa.lista();
		int [][] matriz = { {0,0,0,0,0},
							{0,2,4,0,0},
							{0,6,8,1,0},
							{0,7,3,5,0},
		};
		int res= Programa.Pieza_Atacada(matriz);
		assertEquals(8, res);
		
	}
	
}

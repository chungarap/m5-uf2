package Solucion;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 * <h2> Clase del programa principal </h2>
 * 
 * @author PabloC
 * @version 1-2021
 *
 */

public class Programa {

	static ArrayList <String> Piezas= new ArrayList <String>(); // Lista que contiene el nombre de las piezas
	static ArrayList <Integer> Puntos_Vida= new ArrayList <Integer>(); // Lista que contiene el PV de las piezas
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader= new Scanner(System.in);
		
		int [][] tablero= matriz();
		int Damage= 0;
		
		lista();
		Llenar_matriz(tablero);
		Mostrar_campo(tablero);
		Damage= Calcular_da�o(Zona_Impacto(tablero, Pieza_Atacada(tablero)));
		
		System.out.print(Damage + " " + (Zona_Impacto(tablero, Pieza_Atacada(tablero)).size()));
		System.out.println("");
		Nom_piezas(Zona_Impacto(tablero, Pieza_Atacada(tablero)));
	}
	
	/**
	 * 
	 * <h2> Muestra la matriz </h2>
	 * 
	 * @param matriz - Tablero del juego
	 * 
	 */
	
	public static void Mostrar_campo (int [][] matriz) {
		int i= 0, j= 0;
		
		for (i= 0; i < 4; i++) {
			for (j= 0; j < 5; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.print("\n");
		}
		
	}
	
	/**
	 * 
	 * <h2> Metodo que calcula el da�o generado por la habilidad </h2>
	 * 
	 * @param id_Piezas Piezas que se encuentran alrededor de la pieza fijada
	 * @return Resultado del calculo
	 */
	
	public static int Calcular_da�o (ArrayList<Integer> id_Piezas) { 
		
		int i= 0, j= 0, Resultado= 0; 
		Double Calc_porc= 0.0, Suma= 0.0;
		
		while (i < id_Piezas.size()) {
			j= Puntos_Vida.get(id_Piezas.get(i)-1);
			Calc_porc= j * 0.4;
			Suma= Suma + Calc_porc;
			System.out.println(Puntos_Vida.get(id_Piezas.get(i)-1));
			i++;
		}
		
		Resultado= (int) (Resultado + Suma);

		return Resultado;
	}
	
	/**
	 * 
	 * <h2> Crea el tablero </h2>
	 * 
	 * @return - El tablero del juego
	 */
	
	public static int [][] matriz () {
		int [][] matriz= new int [4][5];
		return matriz;
	}
	
	/**
	 * 
	 * <h2> Llena en 8 posiciones del tablero  </h2>
	 * 
	 * @param matriz - Tablero del juego
	 */
		
	public static void Llenar_matriz (int [][] matriz) {
		
		Random al= new Random();
		int i= 0, j= 0, k= 0, aux= 1;
		
		while (i < Piezas.size()) {
			j= al.nextInt(4);
			k= al.nextInt(5);
			if (matriz[j][k] == 0) {
				matriz[j][k]= aux;
				i++;
				aux++;
			}
		}
	}
	
	/**
	 * 
	 * <h2> Muestra las piezas afectadas </h2>
	 * 
	 * @param id_piezas - Lista de las piezas afectadas
	 */
	
	public static void Nom_piezas(ArrayList <Integer> id_piezas) {
		int i= 0;
		
		while (i < id_piezas.size() - 1) {
			System.out.print(Piezas.get(id_piezas.get(i)-1) + ", ");
			i++;
		}
		System.out.print(Piezas.get(id_piezas.get(i)-1));
	}
	
	/**
	 * <h2> Busca si hay piezas alrededor del objetivo </h2>
	 * 
	 * @param matriz - Tablero del juego
	 * @return - Lista de piezas afectadas
	 * 
	 */
	
	public static ArrayList<Integer> Zona_Impacto (int [][] matriz, int Pieza_Atacada) {
		
		ArrayList <Integer> Piezas_Afectadas= new ArrayList <Integer>();
		
		int i= Pieza_Atacada;
		int j= 0 ,k= 0;
		
		for (j= 0;j < 4; j++) { 
			for (k= 0;k < 5; k++) {
				if (matriz[j][k] == i) {
					Piezas_Afectadas.add(matriz[j][k]);
					if (j-1 >= 0 && matriz[j-1][k] != 0) Piezas_Afectadas.add(matriz[j-1][k]);
					if (j-1 >= 0 && k-1 >= 0 && matriz[j-1][k-1] != 0) Piezas_Afectadas.add(matriz[j-1][k-1]);
					if (j-1 >= 0 && k+1 < 5 && matriz[j-1][k+1] != 0) Piezas_Afectadas.add(matriz[j-1][k+1]);
					if (k+1 < 5 && matriz[j][k+1] != 0) Piezas_Afectadas.add(matriz[j][k+1]);
					if (k-1 >= 0 && matriz[j][k-1] != 0) Piezas_Afectadas.add(matriz[j][k-1]);
					if (j+1 < 4 && k-1 >= 0 && matriz[j+1][k-1] != 0) Piezas_Afectadas.add(matriz[j+1][k-1]);
					if (j+1 < 4 && matriz[j+1][k] != 0) Piezas_Afectadas.add(matriz[j+1][k]);
					if (j+1 < 4 && k+1 < 5 && matriz[j+1][k+1] != 0) Piezas_Afectadas.add(matriz[j+1][k+1]);
				}
			}
		}
		
		return Piezas_Afectadas; 
	}
	
	/**
	 * 
	 * <h2> Localiza la pieza con mas PV </h2>
	 * 
	 * @param matriz - Tablero del juego
	 * @return - El identificador de la pieza
	 * 
	 */
	
	public static int Pieza_Atacada (int [][] matriz) {
		
		int i= 0,j= 0 ,aux= 0 ,id= 0;
		
		for (i= 0;i < 4; i++) { 
			for (j= 0;j < 5; j++) {
				if (matriz[i][j] != 0) {
					if (aux < Puntos_Vida.get(matriz[i][j]-1)) {
						aux= Puntos_Vida.get(matriz[i][j]-1);
						id= matriz[i][j];
					}
				}
			}
		}
		return id;
	}
	
	/**
	 * <h2> Lista de piezas existentes mas sus PV correspondientes </h2> 
	 */
	
	public static void lista () {
		
		Piezas.add("Venomancer");
		Puntos_Vida.add(1050);
		Piezas.add("Reksai");
		Puntos_Vida.add(1110);
		Piezas.add("Espadachin");
		Puntos_Vida.add(1230);
		Piezas.add("Hombre Lobo");
		Puntos_Vida.add(1190);
		Piezas.add("Ogro mago");
		Puntos_Vida.add(1270);
		Piezas.add("Caballero del mal");
		Puntos_Vida.add(1240);
		Piezas.add("Espiritu rocoso");
		Puntos_Vida.add(1100);
		Piezas.add("Devastador");
		Puntos_Vida.add(1300);
		
	}

}
